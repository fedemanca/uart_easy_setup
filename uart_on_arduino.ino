//UART setup with Arduino


//Registers

//UDRn (USART I/O DATA Register): transmit and receive data buffer
//The transmit buffer can only be written when the UDREn flag in the UCSRnA register is set.
//If the flag is set, the data will be moved to the shift register when it is empty

//UCSRnA (USART Control and Status Register A)
//RXCn: set when unread data in buffer. I receiver disabled, RXCN set to zero
//TXCn; set when frame transmitted
//UDREn:if 1, buffer (UDRn) empty, ready to be written
//FEn: set if next character received had error (if first stop bit is zero).
//DORn: data overrun

//UCSRnB:
//bit 7-3: interrupts
//UCSZn2: number of data bits transmitter and receiver use
//....


//UCSRnC: sets parity, asynch/synch, stop, character size, clock polarity

//UBRRnL and UBRRnH (Baud Rate regs): fosc = 1MHz,U2Xn = 0, UBRRn = 6, Baud = 9600

#include <avr/io.h>
#define F_CPU 16000000
#define BAUD 9600 
#include<util/setbaud.h>


void setup() {
  // put your setup code here, to run once:
  USART_Init();

}

void loop() {
  // put your main code here, to run repeatedly:
  USART_Transmit("c");
  delay(100);

}


void USART_Init(){
  //The initialization process normally consists of setting the baud rate, setting frame format and enabling the transmitter or the receiver depending on the usage
  
  
  UBRR0H = 0;
  UBRR0L = 0x00110111; //103
  UCSR0C = 0x00000011;
}

void USART_Transmit(unsigned char data){
  while (!(UCSR0A & (1<<UDRE0))); //wait for buffer to be empty

  UDR0 = data;
}

unsigned char USART_Receive(){

  while (!(UCSR0A & (1<<RXC0))); //wait for data to be transmitted

  return UDR0;


}

