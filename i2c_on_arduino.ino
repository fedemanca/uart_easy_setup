// I2C on Arduino

/*
The TWI protocol allows the systems designer to interconnect up to 128 different devices using 
only two bi-directional bus lines, one for clock (SCL) and one for data (SDA). The only external 
hardware needed to implement the bus is a single pull-up resistor for each of the TWI bus lines. 
All devices connected to the bus have individual addresses, and mechanisms for resolving bus 
contention are inherent in the TWI protocol. The address of a device should be written in the
respective dastasheet (for oled display usually is 0x3D )

Registers:

TWBR – TWI Bit Rate Register: Division factor for bit rate generator, a frequency divider wich generates SCL clock

TWCR - TWI Control Register:
  - TWINT: interrupt flag, set by hardware when finished job. Need to be cleared by software 1 write.
           clearing this flag starts operations
  - TWEA:  controls generation of ack. If 1, ack pulse generated if the slave address has been received or general call or data byte received
  - TWSTA: write to 1 when desire to become master whith start generation
  - TWSTO: wiriting to q generates stop, then cleared automaticall. If in slave mode, not stop generation but returns of unaddressed slave mode
  - TWWC: set when try to write when twint low
  - TWEN: enable twi operations
  - Bit 0: TWI interrupt enable

TWSR - TWI status reg
  - TWS: 5 bits that describe status
  - TWPS: prescaler bits
TWDR - Data reg, not writtable when TWINT is set by hardware
TWAR: TWI (Slave) Address reg: contains address of slave which we are communicating with. The LSB recognize general call address (0x00)
TWAMR - TWI (slave) address mask reg: can disable address bits in the twar.

Different modes: Master/Slave Transimmter/Receiver
Different situations based on the status reg value.





*/
void setup_i2c(){
  TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN); 
  while (!(TWCR & (1<<TWINT))); //wait for twint to be cleared
  if ((TWSR & 0xF8) != START) //if  status different from start, error
    ERROR();
  TWDR = SLA_W; //load slave address (SLA) and Write/Read bit (W)
  TWCR = (1<<TWINT) |(1<<TWEN); //clear twint and set enable to start transmission
  while(!(TCR & (1<<TWINT))); //wait for twint flag set = SLA + W transmitted and ACK received
  if((TWSR & 0xF8) != MT_SLA_ACK)//error is status different from mt_sla_ack
    ERROR();
  // after doing the same steps and transmitting data, we can call the sop condition
  TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWSTO); 
}


void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:

}

