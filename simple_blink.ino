/*
 * GccApplication1.c
 *
 * Created: 04/03/2024 16:27:52
 * Author : utente
 */ 

#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>

//The DDR register, determines whether the pin is an INPUT or OUTPUT. The PORT register controls whether the pin is HIGH or LOW, and the PIN register reads the state of INPUT pins set to input with pinMode()


int main(void)
{
    /* Replace with your application code */
	DDRB = 0xFF;
    while (1) 
    {
		PORTB = 0x00;
		_delay_ms(1000);
		PORTB = 0xFF;
		_delay_ms (3000);
		
    }
}

