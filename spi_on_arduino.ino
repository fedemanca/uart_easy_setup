//SPI setup on Arduino
// The SPI master initiates the communication cycle when pulling low the slave select
//SS pin of the desired slave. Master and slave prepare the data to be sent in their respective shift registers, and the master
//generates the required clock pulses on the SCK line to interchange data. Data is always shifted from master to slave on the
//master out – slave In, MOSI, line, and from slave to master on the master In – slave out, MISO, line. After each data packet,
//the master will synchronize the Slave by pulling high the slave select, SS, line

/*
PORTD maps to Arduino digital pins 0 to 7

  DDRD - The Port D Data Direction Register - read/write

  PORTD - The Port D Data Register - read/write

  PIND - The Port D Input Pins Register - read only

PORTB maps to Arduino digital pins 8 to 13 The two high bits (6 & 7) map to the crystal pins and are not usable

  DDRB - The Port B Data Direction Register - read/write

  PORTB - The Port B Data Register - read/write

  PINB - The Port B Input Pins Register - read only

PORTC maps to Arduino analog pins 0 to 5. Pins 6 & 7 are only accessible on the Arduino Mini

  DDRC - The Port C Data Direction Register - read/write

  PORTC - The Port C Data Register - read/write
  PINC - The Port C Input Pins Register - read only

MOSI, MISO and SCK are pin 11, 12 and 13 on DDRB
*/
//Registers



void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:

}

void SPI_MasterInit(void)
{
  /* Set MOSI and SCK output, all others input */
  DDRB = (1<<11)|(1<<12);
  /* Enable SPI, Master, set clock rate fck/16 */
  SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0); 
}
void SPI_MasterTransmit(char cData)
{
  /* Start transmission */
  SPDR = cData;
  /* Wait for transmission complete */
  while(!(SPSR & (1<<SPIF))) //SPIF set when transmission completed
  ;
}

void SPI_SlaveInit(void)
{
  /* Set MISO output, all others input */
  DDRB = (1<<13);
  /* Enable SPI */
  SPCR = (1<<SPE);
}

char SPI_SlaveReceive(void){

  while(!(SPSR & (1<<SPIF))) //SPIF set when transmission completed
  ;

  return SPDR;

}